<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SummriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('summries')->insert([
            'name' => 'summary',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);                
       
    }

}
